# Assignment2DataPersistenceandAccess
SQL / Databases / C# SQLClient
## Contributors

Esben Bjarnason - https://gitlab.com/EsbenLB

Jonas Svåsand - https://gitlab.com/jsva

## Overview
This is an assignment that consists of two parts, creation of superheroDB and usage (in c#) of the chinookDB.

### SuperheroDB
The superhero database is a database that can be used to store superheroes (their name, alias and origin), superhero assistants (their name and the superhero they assist), and superhero powers (name and description of the power). The  superheroes can also be linked to the powers they possess. This is a database created with SQL queries, which are included.

### Chinook
In this part we used the SQLclient of C# to interact with the chinook database. We have made several methods which you can use to do some CRUD operations on the database. E.g. adding new customers, changing the data of an existing customer, or viewing all customers. There are also some other more advanced operations like getting the customer who has spent the most.

## Instructions:
1. Clone the repository to your computer

### SuperheroDB
2. Navgiate to the cloned repository and open the 'SQL' Folder.
3. Execute the SQL queries within the folder, in the order the filenames suggest. You can do this in a database program or use Microsoft SQL server + Microsoft SQL Server Management Studio.
4. After all the Queries have been executed you can view the databases tables and data by using SQL `SELECT` queries.


### Chinook
2. Open the project in Visual Studio
3. In Repositories/ConnetionStringHelper.cs edit connectionStringBuilder.DataSource value to be the value of your own MSSM hosting a chinook database.
4. You can now call the methods we have provided in Repositories/CustomerRepository.cs, in order to interact with the chinook database.
