﻿using Ass2Data.Exceptions;
using Ass2Data.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ass2Data.Repositories
{
    public class CustomerRepository : ICustomerRepository 
    {
        /// <summary>
        /// Starting sql for every get customer func
        /// </summary>
        string selectCustomerSql = "SELECT CustomerId, FirstName, LastName, Email, Country, PostalCode, Phone FROM Customer ";
        /// <summary>
        /// Read Customer varables from database. Add customer to list. return list
        /// </summary>
        /// <param name="reader"></param>
        /// <returns>List of customer given Sql</returns>
        public List<Customer> ReadCustomer(SqlDataReader reader)
        {
            List<Customer> custList = new();
            while (reader.Read())
            {
                Customer temp = new();
                temp.CustomerId = reader.GetInt32(0);
                temp.FirstName = reader.GetString(1);
                temp.LastName = reader.GetString(2);
                temp.Email = reader.GetString(3);
                // Try catch for values that can be Null in database
                try { temp.Country = reader.GetString(4); }    catch (Exception e) { /* Do nothing */  }
                try { temp.PostalCode = reader.GetString(5); } catch (Exception e) { }
                try { temp.Phone = reader.GetString(6); }      catch (Exception e) {  }
                custList.Add(temp);
            }
            return custList;
        }
        /// <summary>
        /// Get all Customers
        /// </summary>
        /// <exception cref="SqlClientException">Customer could not be retrieved message</exception>  
        /// <returns>List of all customers</returns>
        public List<Customer> GetAllCustomer()
        {
            string sql = selectCustomerSql;
            try
            {
                // Connecting to database
                using (SqlConnection conn = new(ConnetionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Read customer
                            return ReadCustomer(reader);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // throw custom Exception
                throw new SqlClientException("Customer could not be retrieved message : " + e);
            }
        }
        /// <summary>
        /// Get customer by CustomerID.
        /// </summary>
        /// <param name="id"></param>
        /// <exception cref="SqlClientException">Customer with <param name="id"></param> could not be retrieved</exception>  
        /// <returns>Customer or null if does not exist</returns>
        public Customer GetCustomerByID(string id)
        {
            Customer customer = new();
            string sql = selectCustomerSql
                + "Where CustomerId = @CustomerId";
            try
            {
                using (SqlConnection conn = new(ConnetionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        // Replace @CustomerId with id
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            List<Customer> custList = ReadCustomer(reader);
                            if (custList.Count == 1) { return custList[0]; }
                            else
                            {
                                return null;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new SqlClientException("Customer with id: " + id + " could not be retrieved. message : " + e);
            }
        }
        /// <summary>
        /// Get all customer with name like <paramref name="searchname"/>
        /// </summary>
        /// <param name="searchname"></param>
        /// <exception cref="SqlClientException">Customer with name <param name="searchname"></param> could not be retrieved. </exception>  
        /// <returns>List of customer with names LIKE <paramref name="searchname"/></returns>
        public List<Customer> GetAllCustomerByName(string searchname)
        {
            List<Customer> custList = new();
            string sql = selectCustomerSql
                + "Where FirstName + ' ' + LastName LIKE @SearchName";
            try
            {
                using (SqlConnection conn = new(ConnetionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@SearchName", '%' + searchname + '%');
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            return ReadCustomer(reader);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new SqlClientException("Customer with name: " + searchname + " could not be retrieved. message : " + e);

            }
        }
        /// <summary>
        /// Get customer begining at <param name="offset"></param>.
        /// Limit number of customer to <param name="limit"></param>.
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <exception cref="SqlClientException">Could not retrieved customer at OFFSET: <param name="offset"></param>
        /// with LIMIT: <param name="limit"></param> could not be retrieved </exception>  
        /// <returns>List of customer at OFFSET limited to LIMIT</returns>
        public List<Customer> GetPageOfCustomer(int limit, int offset)
        {
            List<Customer> custList = new();
            string sql = selectCustomerSql
                + "ORDER BY CustomerId OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY";
            try
            {
                using (SqlConnection conn = new(ConnetionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@Limit", limit);
                        cmd.Parameters.AddWithValue("@Offset", offset);
                        
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            return ReadCustomer(reader);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new SqlClientException("Could not retrieved customer at OFFSET: " + offset + " with LIMIT: " + limit +  ". message : " + e);

            }
        }
        /// <summary>
        /// Add one customer to database.
        /// </summary>
        /// <param name="customer"></param>
        /// <exception cref="SqlClientException">Could not add customer </exception>  
        /// <returns>Bool: If successfully added customer to database</returns>
        public bool AddCustomer(Customer customer)
        {
            string sql = "Insert INTO Customer(FirstName, LastName, Email, Country, PostalCode, Phone)"
                + "Values(@FirstName, @LastName, @Email, @Country, @PostalCode, @Phone)";
            try
            {
                using (SqlConnection conn = new(ConnetionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);

                        // Return true if customer was added successfully
                        return cmd.ExecuteNonQuery() > 0;
                    }
                }
            }
            catch (Exception e)
            {
                throw new SqlClientException("Could not add customer. message : " + e);

            }
        }
        /// <summary>
        /// Find customer by CustomerID. And update customer values to <param name="customer"></param>.
        /// </summary>
        /// <param name="customer"></param>
        /// <exception cref="SqlClientException">Could not update customer </exception>  
        /// <returns>Bool: If successfully updated customer</returns>
        public bool UpdateCustomer(Customer customer)
        {
            string sql = "UPDATE Customer SET FirstName = @FirstName, LastName = @LastName, Email = @Email,"
               + "Country = @Country, PostalCode= @PostalCode, Phone = @Phone "
                + "WHERE  CustomerId = @CustomerId ";
            try
            {
                using (SqlConnection conn = new(ConnetionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // make a command
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        return cmd.ExecuteNonQuery() > 0;
                    }
                }
            }
            catch (Exception e)
            {
                throw new SqlClientException("Could not update customer. message : " + e);
            }
        }
        /// <summary>
        /// Get all countryes with number of customer pr country
        /// </summary>
        /// <exception cref="SqlClientException">Could not retrieve all countries </exception>  
        /// <returns>List<CustomerCountry>: country name, country number of customer</returns>
        public List<CustomerCountry> GetAllCountries()
        {

            string sql = "SELECT Country, count(Country) AS CountryCount from Customer "
                + "GROUP BY Country ORDER BY CountryCount DESC";
            try
            {
                using (SqlConnection conn = new(ConnetionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            List<CustomerCountry> custList = new();
                            while (reader.Read())
                            {
                                CustomerCountry temp = new();
                                try { temp.CountryName = reader.GetString(0); }  catch (Exception e) { /* Do nothing */ }
                                try { temp.CustomerCount = reader.GetInt32(1); } catch (Exception e) { /* Do nothing */ }
                                custList.Add(temp);
                            }
                            return custList;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new SqlClientException("Could not retrieve all countries. message : " + e);
            }
        }
        /// <summary>
        /// Get a list of customer that spends the most.
        /// </summary>
        /// <exception cref="SqlClientException">Could not retrieve top spenders(customer) </exception>  
        /// <returns>list of customer that spends the most</returns>
        public List<CustomerSpender> GetTopSpenders()
        {
            string sql = "SELECT Invoice.CustomerId, Customer.FirstName, Customer.LastName, "
                        + "Customer.Email, Customer.Country, Customer.PostalCode, Customer.Phone, "
                        + "SUM(Invoice.Total) AS TotalInvoice FROM Invoice "
                        + "INNER JOIN Customer ON Invoice.CustomerId = Customer.CustomerID "
                        + "GROUP BY Invoice.CustomerId, Customer.FirstName, Customer.LastName, "
                        + "Customer.Email, Customer.Country, Customer.PostalCode, Customer.Phone "
                        + "ORDER BY TotalInvoice DESC;";
            try
            {
                using (SqlConnection conn = new(ConnetionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // make a command
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        // reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            List<CustomerSpender> custList = new();
                            while (reader.Read())
                            {

                                Customer temp = new();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Email = reader.GetString(3);
                                try { temp.Country = reader.GetString(4); }    catch (Exception e) { /* Do nothing */ }
                                try { temp.PostalCode = reader.GetString(5); } catch (Exception e) { /* Do nothing */ }
                                try { temp.Phone = reader.GetString(6); }      catch (Exception e) { /* Do nothing */ }

                                decimal totalSPenders = 0;
                                try { totalSPenders = reader.GetDecimal(7); } catch (Exception e) { /* Do nothing */ }
                                CustomerSpender tempSpender = new(totalSPenders,
                                    temp.FirstName, temp.LastName, temp.Country, temp.PostalCode, temp.Phone, temp.Email
                                    );

                                custList.Add(tempSpender);
                            }
                            return custList;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new SqlClientException("Could not retrieve top spenders(customer). message : " + e);
            }
        }
        /// <summary>
        /// Get most popular genre for <param name="customer"></param>.
        /// </summary>
        /// <param name="customer"></param>
        /// <exception cref="SqlClientException">Could not retrieve top genre </exception>  
        /// <returns>CustomerGenre contains list<string> with most popular genre for customer</returns>
        public CustomerGenre GetTopGenre(Customer customer)
        {
            string sql = "SELECT Genre.Name, COUNT(Genre.Name) AS Purchases FROM Invoice "
                        + "INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId "
                        + "INNER JOIN Track ON  InvoiceLine.TrackId = Track.TrackId "
                        + "INNER JOIN Genre ON Track.GenreId = Genre.GenreId "
                        + "WHERE Invoice.CustomerId = @CustomerID "
                        + "GROUP BY Genre.Name "
                        + "ORDER BY Purchases DESC "
                        + "OFFSET 0 ROWS FETCH NEXT 2 ROWS ONLY;";
            try
            {
                using (SqlConnection conn = new(ConnetionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerID", customer.CustomerId);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            
                            List<int> count = new();
                            List<String> names = new();
                            while (reader.Read())
                            {
                                names.Add(reader.GetString(0));
                                count.Add(reader.GetInt32(1));
                            }
                            CustomerGenre customerGenre = new(customer.FirstName, customer.LastName, customer.Country, 
                                customer.PostalCode, customer.Phone, customer.Email);
                            customerGenre.CustomerId = customer.CustomerId;
                            if (count[0] == count[1])
                            {
                                customerGenre.genre = names;
                                return customerGenre;
                            }
                            customerGenre.genre = new List<string>() { names[0] };
                            return customerGenre;


                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new SqlClientException("Could not retrieve top genre. message : " + e);
                
            }
        }
    }
}
