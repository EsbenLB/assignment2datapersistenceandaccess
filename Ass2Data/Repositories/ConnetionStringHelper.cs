﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ass2Data.Repositories
{
    /// <summary>
    /// Retrieves the value with the specified key from the ConnectionStrings section of the configuration source. 
    /// Calling this method is shorthand for GetSection("ConnectionStrings")[name].
    /// </summary>
    /// <returns>GetSection("ConnectionStrings")[name]</returns>
    public class ConnetionStringHelper
    {
        
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder connectionStringBuilder = new();
            // name of sql server 
            connectionStringBuilder.DataSource = "DESKTOP-69A72O5\\SQLEXPRESS";
            // Chinook = database
            connectionStringBuilder.InitialCatalog = "Chinook";
            // Use windows login
            connectionStringBuilder.IntegratedSecurity = true;
            connectionStringBuilder.TrustServerCertificate = true;
            return connectionStringBuilder.ConnectionString;
        }
    }
}
