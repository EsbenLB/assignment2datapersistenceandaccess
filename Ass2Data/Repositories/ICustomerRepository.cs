﻿using Ass2Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ass2Data.Repositories
{
    public interface ICustomerRepository
    {
        public List<Customer> GetAllCustomer();
        public Customer GetCustomerByID(string id);
        public List<Customer> GetAllCustomerByName(string searchname);
        public List<Customer> GetPageOfCustomer(int limit, int offset);
        public bool AddCustomer(Customer customer);
        public bool UpdateCustomer(Customer customer);
        public List<CustomerCountry> GetAllCountries();
        public List<CustomerSpender> GetTopSpenders();
        public CustomerGenre GetTopGenre(Customer customer);
    }
}
