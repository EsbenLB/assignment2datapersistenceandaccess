﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ass2Data.Exceptions
{
    class SqlClientException : Exception
    {
        public SqlClientException()
        {
        }

        public SqlClientException(string message)
            : base(message)
        {
        }

        public SqlClientException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
