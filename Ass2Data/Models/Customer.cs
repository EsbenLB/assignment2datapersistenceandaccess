﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ass2Data.Models
{   
    /// <summary>
    /// Represent customer in chinook database
    /// </summary>
    public class Customer
    {
        public int CustomerId  { get; set; }
        public string FirstName  { get; set; }
        public string LastName  { get; set; }
        public string Country  { get; set; }
        public string PostalCode { get; set; }
        public string Phone  { get; set; }
        public string Email  { get; set; }

        public Customer(string FirstName = "", string LastName = "", string Country = "", 
            string PostalCode = "", string Phone = "", string Email = ""
            )
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Country = Country;
            this.PostalCode = PostalCode;
            this.Phone = Phone;
            this.Email = Email;
        }
        /// <summary>
        /// Display customer information
        /// </summary>
        /// <returns>Customer info</returns>
        public override string ToString()
        {
            string info = "";
            info += "CustomerId: " + CustomerId.ToString() + "\n";
            info += "FirstName: " + FirstName + "\n";
            info += "LastName: " + LastName + "\n";
            info += "Country: " + Country + "\n";
            info += "PostalCode: " + PostalCode + "\n";
            info += "Phone: " + Phone + "\n";
            info += "Email: " + Email + "\n";
            return info;
        }
    }
}
