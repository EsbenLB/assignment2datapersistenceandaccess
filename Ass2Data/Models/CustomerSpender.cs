﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ass2Data.Models
{
    /// <summary>
    /// Customer and amount spend
    /// </summary>
    public class CustomerSpender : Customer
    {
        public decimal totalInvoice;

        public CustomerSpender(decimal totalInvoice, string FirstName = "", string LastName = "", 
            string Country = "", string PostalCode = "", string Phone = "", string Email = ""
            ) :base(FirstName, LastName, Country, PostalCode, Phone, Email)
        {
            this.totalInvoice = totalInvoice;
        }
        public override string ToString()
        {
            string info = "";
            // Customer
            info += "CustomerId: " + CustomerId.ToString() + "\n";
            info += "FirstName: " + FirstName + "\n";
            info += "LastName: " + LastName + "\n";
            info += "Country: " + Country + "\n";
            info += "PostalCode: " + PostalCode + "\n";
            info += "Phone: " + Phone + "\n";
            info += "Email: " + Email + "\n";
            // TotalInvoice
            info += "totalInvoice: " + totalInvoice + "\n";
            return info;
        }
    }
}
