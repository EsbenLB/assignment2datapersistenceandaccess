﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ass2Data.Models
{
    /// <summary>
    /// Represent a country and amount of customer from this country in chinook database
    /// </summary>
    public class CustomerCountry
    {
        public string CountryName { get; set; }
        public int CustomerCount { get; set; }
        /// <summary>
        /// Display Customer Country information
        /// </summary>
        /// <returns>CustomerCountry info</returns>
        public override string ToString()
        {
            string info = "";
            info += "CountryName: " + CountryName + "\n";
            info += "CustomerCount: " + CustomerCount + "\n";
            return info;
        }
    }
    
}
