﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ass2Data.Models
{
    /// <summary>
    /// Customer with their favorite genre
    /// </summary>
    public class CustomerGenre : Customer
    {
        public List<string> genre;
        public CustomerGenre(string FirstName = "", string LastName = "",
            string Country = "", string PostalCode = "", string Phone = "", string Email = ""
            ) : base(FirstName, LastName, Country, PostalCode, Phone, Email)
        {
        }
        /// <summary>
        /// Display Customer and Customer Genre information
        /// </summary>
        /// <returns>Customer and CustomerGenre info</returns>
        public override string ToString()
        {

            string info = "";
            // Customer
            info += "CustomerId: " + CustomerId.ToString() + "\n";
            info += "FirstName: " + FirstName + "\n";
            info += "LastName: " + LastName + "\n";
            info += "Country: " + Country + "\n";
            info += "PostalCode: " + PostalCode + "\n";
            info += "Phone: " + Phone + "\n";
            info += "Email: " + Email + "\n";
            // Most popular genre
            for (int i = 0; i < genre.Count; i++)
            {
                info += "genre: " + genre[i] + "\n";
            }
            return info;
        }
    }
}
