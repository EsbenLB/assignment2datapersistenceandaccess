﻿using Ass2Data.Models;
using Ass2Data.Repositories;
using System;
using System.Collections.Generic;

namespace Ass2Data
{
    class Program
    {
        static void Main(string[] args)
        {
            
            //TestGetCustomer();
            //TestGetCustomerByIdAndName();
            //TestPageOfCustomer();
            //TestAddCustomer();
            //TestUpdateCustomer();
            //TestCountryCustomer();
            //TestHighSpender();
            TestPopularGenre();

        }
        /// <summary>
        /// Test functions in CustomerRepository
        /// </summary>
        static void TestGetCustomer()
        {
            CustomerRepository repos = new();
            List<Customer> custList = repos.GetAllCustomer();
            for (int i = 0; i < custList.Count; i++)
            {
                Console.WriteLine(custList[i].ToString());
            }
        }
        static void TestGetCustomerByIdAndName()
        {
            CustomerRepository repos = new();
            Console.WriteLine(repos.GetCustomerByID("1").ToString());
            Console.WriteLine(repos.GetAllCustomerByName("Helena")[0].ToString());
        }
        static void TestPageOfCustomer()
        {
            CustomerRepository repos = new();
            List<Customer> custList = repos.GetPageOfCustomer(2, 2);
            for (int i = 0; i < custList.Count; i++)
            {
                Console.WriteLine(custList[i].ToString());
            }
        }
        static void TestAddCustomer()
        {
            CustomerRepository repos = new();
            Console.WriteLine("BoBion: " + repos.GetAllCustomerByName("BoBion").Count);
            Console.WriteLine(repos.AddCustomer(new("BoBion")));
            Console.WriteLine("BoBion: " + repos.GetAllCustomerByName("BoBion").Count);
        }
        static void TestUpdateCustomer()
        {
            CustomerRepository repos = new();
            Customer test = repos.GetCustomerByID("1");
            Console.WriteLine(test.ToString());
            test.FirstName = "UpdatedName";
            Console.WriteLine(repos.UpdateCustomer(test));
            Console.WriteLine(repos.GetCustomerByID("1").ToString());
        }
        static void TestCountryCustomer()
        {
            CustomerRepository repos = new();

            List<CustomerCountry> counList = repos.GetAllCountries();
            for (int i = 0; i < counList.Count; i++)
            {
                Console.WriteLine(counList[i].ToString());
            }
        }
        static void TestHighSpender()
        {
            CustomerRepository repos = new();

            List<CustomerSpender> counList = repos.GetTopSpenders();
            for (int i = 0; i < counList.Count; i++)
            {
                Console.WriteLine(counList[i].ToString());
            }
        }
        static void TestPopularGenre()
        {
            CustomerRepository repos = new();
            Customer test = repos.GetCustomerByID("11");
            CustomerGenre genre = repos.GetTopGenre(test);
            foreach (var item in genre.genre)
            {
                Console.WriteLine(item);
            } 
        }

    }

}
