USE Superheroesdb

CREATE TABLE SuperheroPower (
superheroID int FOREIGN KEY REFERENCES Superhero(ID),
powerID int FOREIGN KEY REFERENCES Power(ID)
PRIMARY KEY (superheroID, powerID)
)