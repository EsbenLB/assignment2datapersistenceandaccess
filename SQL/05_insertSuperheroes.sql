USE Superheroesdb

INSERT INTO Superhero (SuperheroName, Alias, Origin)
VALUES ('Superman', 'Clark Kent', 'Krypton'), 
		('Batman', 'Bruce Wayne', 'Bats'),
		('Spiderman', 'Peter Parker', 'Spider');