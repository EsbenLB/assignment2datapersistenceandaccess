USE Superheroesdb
INSERT INTO Power (PowerName, description)
VALUES  ('Flight', 'Allows you to fly'),
		('Strength', 'Super Strength'),
		('Money', 'Allows you to buy gadgets'),
		('SpideySense', 'Power of foresight');


INSERT INTO SuperheroPower (superheroID, powerID)
VALUES  (1,1),
		(1,2),
		(2,3),
		(3,2),
		(3,4);